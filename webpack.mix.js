let mix = require('laravel-mix');

// let bootstrap = require('bootstrap');

//bootstrap.sc
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/assets/flags', 'public/img/flags');
// mix.copyDirectory('resources/assets/font', 'public/fonts');
// mix.copyDirectory('resources/assets/font/fontAwesome', 'public/fonts');


mix.js([

    // 'resources/assets/js/bootstrap-select.min.js',
    // 'resources/assets/js/jquery.js',
    // 'resources/assets/js/jquery-ui.js',

    // 'resources/assets/js/popper.js',
    // 'resources/assets/js/bootstrap.js',
    // 'resources/assets/js/chart.js',
    // 'resources/assets/js/fontawesome-all.js',
    'resources/assets/js/custom.js',
    'resources/assets/js/script.js'
], 'public/js/script.js').version();





// mix.styles('resources/assets/css/bootstrap.css', 'public/css/bootstrap.css').version();


mix.styles([
    // 'resources/assets/js/jquery.js',
    'resources/assets/css/bootstrap.css',
    // 'resources/assets/css/jquery-ui.css',
    'resources/assets/css/colors.css',
    // 'resources/assets/css/bootstrap-select.min.css',
    'resources/assets/css/style.css'

], 'public/css/style.css').sourceMaps().version();

















//////////////////////////////////////////////////////////////////
