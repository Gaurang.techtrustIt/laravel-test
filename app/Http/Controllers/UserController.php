<?php
namespace App\Http\Controllers;
use Config;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use \Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;


class UserController extends Controller
{
    /**
     * regiter new user view file.
     * @param  $request
     * @return type
     */
    public function register($id)
    {
        $userInfo = User::where('id', '=',(int)$id)->first();
    	return view('userRegister', compact('id'));
    }

    /**
     * update user username and password.
     * Send authentication code.
     * @param  $request
     * @return type
     */
    public function addUser(Request $request)
    {
        $data = $request->input();
        /* Start Code for validation. */
            $validatedData = $request->validate
            ([
                'username' 		=> 'required|string|min:4|max:20',
                'password' 		=> 'required|string|max:20',
            ]);
        /* End Code for validation. */
        if (!$validatedData) 
        {
           flash("Something went wrong")->warning();
            return back();
        }
        /* Start Code for update information username,password. */
            $user 							= User::find((int)$data['id']);
            //Get user name.
            $userEmail 						= $user['email'];
            $user->user_name 				= $data['username'];
            $user->password 				= bcrypt($data['password']);
            $user->authenticate_code 		= rand();                
            $user->save();
        /* End Code for update information username,password. */
        /* Start Code for send email. */
            $emailSubject   =   '';
            $toEmails       =   array(); 
            $mailText       =   ''; 
            $mailText       =   "Hi User,\r\n\n";
            $emailSubject   =   "User Registration Code";
            $mailText      .=   "Authentication Code : ".$user->authenticate_code."\r\n\n";
            $toEmailIds     =   $userEmail;


            $transport  = (new SmtpTransport('smtp.gmail.com', '587', 'tls'))->setUsername('testing.jignesh16@gmail.com')->setPassword('qeuuxhguatzvewbu');

            $mailer     = new Swift_Mailer($transport);
            Mail::setSwiftMailer($mailer);
            $mail = Mail::raw($mailText, function ($m) use ($emailSubject,$mailText,$toEmailIds) 
            {
                $m->subject($emailSubject);
                $m->from('testing.jignesh16@gmail.com', 'E-Mail');
                $m->to($toEmailIds);
            });                    
        /* End Code for send email. */

        $message = "Send authentication code in your Email.";
        return view('viewMessage', compact('message'));
    }
}
