<?php
namespace App\Http\Controllers\Api;
use Validator;
use File;
use Image; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Controllers\Controller;
use App\User;
use \Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;

class UserApiController extends APIBaseController 
{
    /**
     * Intialize all base details
     */
    public function __construct(Request $request)
    {
        $this->userRegisteredMessage      = "User register successfully.";        
        $this->userExistMessage           = "E-Mail ID already exist.";        
        $this->sendEmailMessage           = "Sent E-Mail Successfully.";        
        $this->UnauthentiMessage          = "Invalid Authentication Code.";        
        $this->UndefinedUserIdMessage     = "User details not exist in database.";        
        $this->UserUpdatedMessage         = "User information updated successfully.";        
    }

    /**
     * regiter new user.
     * @param  $request
     * @return type
     */
    public function userSignUp(Request $request) 
    {
        $response_data = 
        [
            'status'      => 1,  
            'message'     => '',
            'errors'      => [],
            'data'        => []
        ];
        $response_code = $this->successCode;
        try 
        {            
            $userFormInputs = $request->all();
            $validator = Validator::make($userFormInputs,
            [
                'email' => ['required','email'],
                'role'  => ['required','integer'],
            ]);
            if($validator->fails()) 
            {
                $errors = $validator->errors();
                $messages = implode(",",$validator->messages()->all());
                throw new HttpResponseException(response()->json([
                    'errors' => $errors,
                    'message' => $messages,
                    'status'=>0
                ], Response::HTTP_UNPROCESSABLE_ENTITY));
            }
            $checkUserResposne = $this->CheckUserExist($userFormInputs,1);
            //If found if condition execute otherwise else if not found.
            if($checkUserResposne == "true")
            {
                $response_data = 
                [
                    'status'      => 0,
                    'message'     => $this->userExistMessage,
                ];
                return $this->sendResponseAPI($response_data,  $this->errorCode);
            }
            else
            {
                $generateNumber      = rand();
                $data['aCode']       = $generateNumber;
                $userId = User::create
                ([
                    'email'                 => $userFormInputs['email'],
                    'role_id'               => $userFormInputs['role'],
                    'registered_at'         => date('Y-m-d H:i:s'),
                    'created_at'            => null,
                    'updated_at'            => null,
                ]);
                $userRegisteredLink = url('/user/register').'/'.$userId['id'];
                /* Start Code for send email. */
                    $emailSubject   =   '';
                    $toEmails       =   array(); 
                    $mailText       =   ''; 
                    $mailText       =   "Hi User,\r\n\n";
                    $emailSubject   =   "New User Registration Link";
                    $mailText      .=   "Registration Link : ".$userRegisteredLink."\r\n\n";
                    $toEmailIds     =   $userFormInputs['email'];


                    $transport  = (new SmtpTransport('smtp.gmail.com', '587', 'tls'))->setUsername('testing.jignesh16@gmail.com')->setPassword('qeuuxhguatzvewbu');

                    $mailer     = new Swift_Mailer($transport);
                    Mail::setSwiftMailer($mailer);
                    $mail = Mail::raw($mailText, function ($m) use ($emailSubject,$mailText,$toEmailIds) 
                    {
                        $m->subject($emailSubject);
                        $m->from('gaurang.techtrustitsolutions@gmail.com', 'E-Mail');
                        $m->to($toEmailIds);
                    });                    
                /* End Code for send email. */
                $response_data = 
                [
                    'message'               =>  $this->sendEmailMessage,
                    'link'                  =>  $userRegisteredLink
                ];
                $response_code = $this->successCode;
                return $this->sendResponseAPI($response_data, $response_code);                
            }  
        }
        catch (Exception $ex) 
        {
            return $this->sendResponseAPI([], $this->errorCode);
        }
    }

    /**
     * check email exist or not.
     * @param  $request
     * @return type
     * Flag = 1 ,Check Email Id Only
     * Flag = 2 ,Check Email and Authentication code Only
     */
    public function CheckUserExist($userInfoArray,$flag)
    {
        if($flag == 1)
        {
            $userInfo = User::where('email', '=', $userInfoArray['email'])->first();
        }
        else
        {
            $userInfo = User::where('email', '=', $userInfoArray['email'])->where('authenticate_code', '=', $userInfoArray['authenticate_code'])->first();
        }

        if($userInfo)
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }

    /**
     * authenticate user code.
     * @param  $request
     * @return type
     */
    public function userAuthenticateCode(Request $request) 
    {
        $response_data = 
        [
            'status'      => 1,  
            'message'     => '',
            'errors'      => [],
            'data'        => []
        ];
        $response_code = $this->successCode;
        try 
        {            
            $userFormInputs = $request->all();
            $validator      = Validator::make($userFormInputs,
            [
                'email'                 => ['required','email'],
                'authenticate_code'     => ['required','integer'],
            ]);
            if($validator->fails()) 
            {
                $errors     = $validator->errors();
                $messages   = implode(",",$validator->messages()->all());
                throw new HttpResponseException(response()->json([
                    'errors' => $errors,
                    'message' => $messages,
                    'status'=>0
                ], Response::HTTP_UNPROCESSABLE_ENTITY));
            }
            $checkUserResposne = $this->CheckUserExist($userFormInputs,2);
            //If found if condition execute otherwise else if not found.
            if($checkUserResposne == "false")
            {
                $response_data = 
                [
                    'status'      => 0,
                    'message'     => $this->UnauthentiMessage,
                ];
                
                return $this->sendResponseAPI($response_data, $response_code);     
                
            }
            else
            {
                //Get user id from database.
                $userInfo = User::select('id')->where('email', '=', $userFormInputs['email'])->where('authenticate_code', '=', $userFormInputs['authenticate_code'])->first();
                //Update the created at date in database.
                $user                           = User::find((int)$userInfo['id']);
                $user->authenticate_code        = "";
                $user->created_at               = date('Y-m-d H:i:s');
                $user->updated_at               = null;
                $user->save();
                //API Response information.
                $response_data = 
                [
                    'status'                =>  1,
                    'message'               =>  $this->userRegisteredMessage
                ];
                $response_code = $this->successCode;
                return $this->sendResponseAPI($response_data,  $this->errorCode);          
            }  
        }
        catch (Exception $ex) 
        {
            return $this->sendResponseAPI([], $this->errorCode);
        }
    }

    /**
     * user information code.
     * @param  $request
     * @return type
     */
    public function userUpdate(Request $request) 
    {
        $response_data = 
        [
            'status'      => 1,  
            'message'     => '',
            'errors'      => [],
            'data'        => []
        ];
        $response_code = $this->successCode;
        try 
        {            
            $userFormInputs = $request->all();
            $validator      = Validator::make($userFormInputs,
            [
                'id'                => ['required','integer'],
                'username'          => ['string','min:4','max:20'],
                'password'          => ['string','max:20'],
                'email'             => ['email'],
                'role'              => ['integer'],
                'avatar'            => ['mimes:jpeg,jpg,png','max:1000']
            ]);
            if($validator->fails()) 
            {
                $errors     = $validator->errors();
                $messages   = implode(",",$validator->messages()->all());
                throw new HttpResponseException(response()->json([
                    'errors' => $errors,
                    'message' => $messages,
                    'status'=>0
                ], Response::HTTP_UNPROCESSABLE_ENTITY));
            }
            //Get user information from database.
            $UserInfo = User::find($userFormInputs['id']);
            //If not found if condition execute otherwise else if not found.
            if(!$UserInfo)
            {
                //API Response information.
                 $response_data = 
                [
                    'status'      => 0,
                    'message'     => $this->UndefinedUserIdMessage,
                ];
                return $this->sendResponseAPI($response_data, $response_code);                
            }
            else
            {
                //Check email id already exist or not with other user details.
                /*
                 * If Found return if condition.(User Email id exists)
                 * Otherwise else condition(User Email id does not exists)
                */
                $checkEmailExist = User::where('email', '=', $userFormInputs['email'])->where('id', '!=', $userFormInputs['id'])->first();
                if(!$checkEmailExist)
                {
                    $filename = "";
                    if($request->hasfile('avatar'))
                    {
                        $avatar     = $request->file('avatar');
                        $filename   = time() . '.' . $avatar->getClientOriginalExtension();
                        Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/avatars/' . $filename));
                    }

                    /* Start code to update the user information. */
                        if(isset($userFormInputs['username']) && $userFormInputs['username'] != '')
                            $UserInfo->user_name            = trim($userFormInputs['username']);

                        if(isset($userFormInputs['password']) && $userFormInputs['password'] != '')
                            $UserInfo->password             = bcrypt(trim($userFormInputs['password']));

                        if(isset($userFormInputs['email']) && $userFormInputs['email'] != '')
                            $UserInfo->email                = trim($userFormInputs['email']);

                        if(isset($userFormInputs['role']) && $userFormInputs['role'] != '')
                            $UserInfo->user_role            = (int)trim($userFormInputs['role']);

                        if($request->hasfile('avatar'))
                            $UserInfo->profile_image        = $filename;
                        
                        $UserInfo->updated_at               = date('Y-m-d H:i:s');
                        $UserInfo->save();
                    /* End code to update the user information. */
                    //API Response information.
                    
                    $response_data = 
                    [
                        'status'                => 1,
                        'message'               =>  $this->UserUpdatedMessage
                    ];
                    $response_code = $this->successCode;                    
                    return $this->sendResponseAPI($response_data,  $this->errorCode);  
                }
                else
                {
                    $response_data = 
                    [
                        'status'      => 0,
                        'message'     => $this->userExistMessage,
                    ];
                    return $this->sendResponseAPI($response_data,  $this->errorCode);
                }     
            }  
        }
        catch (Exception $ex) 
        {
            return $this->sendResponseAPI([], $this->errorCode);
        }
    }
}