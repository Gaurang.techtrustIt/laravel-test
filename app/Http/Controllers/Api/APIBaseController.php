<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class APIBaseController extends Controller
{
    //Login User Data
    protected $user;
    
    //Send Response Success and Error data
    protected $errorCode = Response::HTTP_UNPROCESSABLE_ENTITY;
    protected $successCode = Response::HTTP_OK;
    
    
    /**
     * Intialize all base details
     */
    public function __construct(Request $request)
    {
        $this->user = $request->get('user');
        if(empty($this->user) && $request->header('token'))
        {
            $this->getAuthUser($request);
        }
    }
    
    /**
     * Login User details
     * @param Request $request
     * @return App/User Object
     */
    public function getAuthUser(Request $request)
    {
        return $this->user = JWTAuth::toUser($request->header('token'));
    }
    
    /**
     * Send API Response
     * @param Mixed $responseData - Send response array
     * @param type $responseCode - Response code
     * @param type $headerData - Response Header data
     * @return type
     */
    public function sendResponseAPI($responseData,$responseCode,$headerData = [])
    {
        if(!isset($responseData['status']))
        {
            if($responseCode == $this->successCode)
            {
                $responseData['status'] = 1;
            }
            else
            {
                $responseData['status'] = 0;
            }
        }
        $responseObj = response()->json($responseData,$responseCode);
        if(!empty($headerData))
        {
            foreach($headerData as $key=>$header)
            {
                $responseObj->header($key, $header);
            }
        }
        return $responseObj;
    }
}
