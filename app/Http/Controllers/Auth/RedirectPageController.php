<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectPageController extends Controller
{
    public function completes()
    {
        $description = "You are successfully attempt this survey.";
        return view('redirectPages.messageView', compact('description'));
    }
    public function disqualified()
    {
        $description = "You are Disqualified this survey.";
        return view('redirectPages.messageView', compact('description'));
    }
    public function quotafull()
    {
        $description = "You are Quotafull this survey.";
        return view('redirectPages.messageView', compact('description'));
    }
    public function qualityterminate()
    {
        $description = "You are Quality Terminate this survey.";
        return view('redirectPages.messageView', compact('description'));
    }
}
