<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createProjectFormPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'epo_number' => 'required',
            'project_name' => 'required',
            'project_reference_name' => 'required',
            'project_type' => 'required',
            'client_id' => 'required|numeric',
            'cpi' => 'required|numeric',
            'loi' => 'required|numeric',
            'ir' => 'numeric',
            'completes' => 'numeric',
            'country' => 'required',
            'project_manager_id' => 'required|numeric',
            'status' => 'required|numeric',
            'unique_pid' => 'boolean',
            'unique_ip' => 'boolean',
            'pre_screener' => 'numeric',
            'unique_link' => 'boolean',
            'multiple_survey_link' => 'numeric',
            'country_restrict' => 'boolean',
            /*'loi_limit' => 'required|numeric',*/
            'speeder_limit' => 'required|numeric',
            'po_number' => 'required',
            'include_survalidate'=>'boolean',
            'project_subject_line'=>'required',
        ];
    }
}
