<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCompanyFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'company_name'                      => 'required',            
            'company_type'                      => 'required',
            'company_country'                   => 'required',        
            'company_address'                   => 'required',
            'company_primary_email'             => 'required|email',
            'company_secondary_email'           => 'nullable|email',
            'company_primary_contact_number'    => 'nullable|regex:/[0-9]/',
            'company_secondary_contact_number'  => 'nullable|regex:/[0-9]/',  
            'note'                              => 'nullable|string',
            'completion_link'                   => 'nullable|url',
            'quotafull_link'                    => 'nullable|url',
            'disqualify_link'                   => 'nullable|url',
            'terminate_link'                    => 'nullable|url',
            'payment_cycle'                     => 'required|integer',
            'currency'                          => 'required|string'
        ];
    }
}
