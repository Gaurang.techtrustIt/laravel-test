<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


class VendorCurrencyListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validationRule = [
            'vendor_id'         => ['required'],
            'vendor_secret_key' => ['required'],
        ];
        return $validationRule;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator  $validator
     *
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $messages = implode(",",$validator->messages()->all());
        throw new HttpResponseException(response()->json([
            'errors' => $errors,
            'message' => $messages,
            'status'=>0
        ], Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
