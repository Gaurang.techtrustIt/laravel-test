<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Mail;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\Mail\ExceptionOccured;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        /*if ($this->shouldReport($exception)) 
        {
            $this->sendEmail($exception); // sends an email
        }*/
        return parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
        /*if ($this->isHttpException($exception)) 
        {
            if ($exception->getStatusCode() == 404) 
            {
                return response()->view('errorPages.' . '404', [], 404);
            }
        }
        return parent::render($request, $exception);*/
    }
    /**
     * [sendEmail send email if error occured in portal.]
     * @author Gaurang Dangi
     * @Created Date      2019-12-27T20:00:09+0530
     * @param   Exception $exception               [description]
     * @return  [type]                             [description]
     */
    public function sendEmail(Exception $exception)
    {
        try 
        {
            $e = FlattenException::create($exception);
            $handler = new SymfonyExceptionHandler();
            $html = $handler->getHtml($e);
            $subject = "Test- Exception Occured!";

            mail::to('gaurang@itoneclick.com')->send(new ExceptionOccured($html));
        } 
        catch (Exception $ex) 
        {
            dd($ex);
        }
    }
}
