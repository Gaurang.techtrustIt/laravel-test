<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\GeneratedInvoices;
use Carbon\Carbon;


class Paymentoverdue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:overdue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Payment overdue if date has gone';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $paymentData=GeneratedInvoices::select('contact_info','id')->whereNotIn('status',[0,3,5,1])->get();
        
        if(!$paymentData->isEmpty()) 
        {
           foreach ($paymentData as $key => $payment) 
          { 
            $paymentDate[$key]=$payment->getInvoiceDueDate();
            $id[$key]=$payment->id;
            $date[$key]       =strtotime($paymentDate[$key]);
            $dueDate[$key]    = date('Y-m-d',$date[$key]);
            $today = Carbon::today()->toDateString();
            if($today > $dueDate[$key])
            {
                $statusUpdate[$key]=GeneratedInvoices::where('id',$id[$key])->where('status','!=',5)->update(['status' =>4]);
            }
            
          }
           $this->info('payment:overdue Cummand Run successfully!');
        }

    }
}
