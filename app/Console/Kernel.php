<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Project;
use App\Indexer;
use GuzzleHttp\Client;
use App\CurrencyRate;
use Carbon\Carbon;
use App\vendorInvoiceInfo;
use App\SurveyAttempts;
use App\RecontactSurveyAttmepts;
use Helper;
use App\VendorProject;
use App\InvoiceRecords;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Request;

class Kernel extends ConsoleKernel
{
    private $access_key = 'f88f4cdead39c8e789a511b227f31214'; //CurrencyRate API access key
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

         Commands\Paymentoverdue::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    { 
        // if (env('APP_ENV') != 'local')
            // $schedule->call($this->updateIndexerJob())->everyMinute();

            $etc2 = $this;

            if(env('APP_ENV') != 'local')
            {
                /*$schedule->call(function () use ($etc2){
                    $this->updateIndexerJob();
                    sleep(10);
                    $this->updateIndexerJob();
                    sleep(10);
                    $this->updateIndexerJob();
                    sleep(10);
                    $this->updateIndexerJob();
                    sleep(10);
                    $this->updateIndexerJob();
                })->everyFifteenMinutes();*/
            }
            else
            {
                /*$schedule->call(function () use ($etc2){
                    $this->updateIndexerJob();
                })->everyFifteenMinutes();*/
            }
            /**
             * Function for change survey Attempt status From In Survey to Abandon
             */
            $schedule->call(function () use ($etc2){
                //$this->getChangeStatus();
                //$this->changeVendorInvoiceStatus();
                //$this->updateProjectIndexerDetails();
                $this->updateVendorStatus();
            })->everyFifteenMinutes();
            

/*            $schedule->call(function () use ($etc2){
                $etc2->updateCurrencyRate();
            })->daily();*/
            /* Cron set for Send Email Information about vendors completes to Project manager and Operation team.*/
            $schedule->call(function () use ($etc2)
            {
                //$this->CronPjctVendorQuatasCompletes();
            })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    private function updateIndexerJob()
    {
        ini_set('max_execution_time', 9999999);
        $projects = Project::where('status',2)->where("created_at",">", Carbon::now()->subMonths(6))->get();

        if ($projects->count() > 0) {
            foreach ($projects as $project) {
                $indexer = Indexer::where('project_id', $project->project_id)->first();

                if ($indexer == null) {
                    $indexer = new Indexer;
                    $indexer->project_id = $project->project_id;
                }

                $valuesForIndexer = [
                    'project_id'    =>$project->project_id,
                    'project_full_name' => $project->invoiceRecord()->first()->project_name . '_' . $project->project_reference_name,
                    'avg_loi' => $project->avgLoi($project->is_recontact),
                    'completes_count' => $project->completeAttemptsCount($project->is_recontact),
                    'hits_count' => $project->surveyAttemptsCount($project->is_recontact),
                    'quotafull_count' => $project->quotafullAttemptsCount($project->is_recontact),
                    'abandon_count' => $project->abandonAttemptsCount($project->is_recontact),
                    'blocked_count' => $project->blockedAttemptsCount($project->is_recontact),
                    'disqualified_count' => $project->disqualifiedAttemptsCount($project->is_recontact),
                    'last_complete' => $project->lastComplete($project->is_recontact)->first()['created_at'],
                    'last_redirect' => $project->lastAttempt($project->is_recontact)->first()['created_at'],
                    'revenue' => $project->totalRevenueNew($project->is_recontact),
                    'revenue_USD' => $project->totalRevenueNew($project->is_recontact) * $project->currency_rate,
                    //'vendor_cost' => $project->totalVendorsCostNew($project->is_recontact),
                    //'vendor_cost_USD' => $project->totalVendorsCostNew($project->is_recontact) * $project->currency_rate,
                    'vendor_cost' => $project->totalVendorsCostInfo($project->is_recontact),
                    'vendor_cost_USD' => $project->totalVendorsCostInfo($project->is_recontact) * $project->currency_rate,
                    'profit' => $project->projectProfitNew($project->is_recontact),
                    'profit_USD' => $project->projectProfitNew($project->is_recontact) * $project->currency_rate,
                    'manager_id' => $project->project_manager_id,
                    'approved_count' => ($project->approvedCompleteAttemptsCount($project->is_recontact)+$project->extra_ids_count),
                    'avg_cpc' => $project->avgCpc($project->is_recontact),
                    'invoice_id' => $project->invoice_id,
                    'ir' => $project->calculateIR($project->is_recontact),
                    'avg_disqualified_loi' => $project->avgDisqualifiedAttemptsLoi($project->is_recontact)
                ];
                $indexer->fill($valuesForIndexer);
                $indexer->save();
            }
            return "ok";
        }
        return "not ok";
    }

    private function updateCurrencyRate()
    {
        $query = 'access_key=' . $this->access_key;

        $currencyRates =json_decode($this->makeApiCall('', $query))->quotes;

        foreach($currencyRates as $currency => $rate)
        {
            $currencyRate = CurrencyRate::where('base', 'USD')->where('currency', substr($currency, 3))->first();

            if(!$currencyRate)
                $currencyRate = new CurrencyRate;
            
                $currencyRate->base = 'USD';
            $currencyRate->currency = substr($currency, 3);
            $currencyRate->rate = $rate;
            if(!$currencyRate->save())
                return 'Failed';
        }
        return "Successfull";
    }

    private function makeApiCall($uri = '', $query = '', $type = 'GET')
    {
        $client = new Client([
            'base_uri' => "http://www.apilayer.net/api/live",
            'query' => $query,
            'headers' => [
                'access_key' => 'f88f4cdead39c8e789a511b227f31214',
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
                'timeout' => 2.0,
            ],

        ]);
        $response = $client->request($type, $uri);
        return $response->getBody()->getContents();
    }
    /**
     * [getChangeStatus Change the respondent status from InSurvey To Abandon]
     * @author Gaurang Dangi
     * @Created Date   2019-07-03T17:07:01+0530
     * @return  [type] [description]
     */
    public function getChangeStatus()
    {
        $totalRecords = 0;
        //Get Projects from database which status is 2 project id desc.
        $projects = Project::select('project_id','is_recontact','loi')->whereIn('status',[2,3])->where("created_at","<", Carbon::now())->orderby('project_id','desc')->get();
        //Check Count if Count is more than 0.
        if ($projects->count() > 0) 
        {
            //Execute the foreach loop.
            foreach ($projects as $project) 
            {
                //Get the data from database which status is 14.
                $getSurveyAttempt = $project->getSurveyAttempts($project['is_recontact']);
                //Table name for update the status.
                $tbleName = ($project['is_recontact'] == 0)?'survey_attempts':'recontact_survey_attmepts';
                //Check Counts, If count is more than 0
                if(count($getSurveyAttempt)>0)
                {
                    //Execute the foreach loop.
                    foreach ($getSurveyAttempt as $getSurveyAttemptInfo) 
                    {
                        //Get the calculate LOI Of Particular respondent.
                        $calculateLOI = $this->calculateLoi($getSurveyAttemptInfo['created_at']);
                        if($calculateLOI > ($project['loi']*5))
                        {
                            $data= array('status'=>1);
                            if($tbleName == "recontact_survey_attmepts")
                            {
                                //Getting RecontactSurveyAttempt entry by $surveyAttemptID
                                RecontactSurveyAttmepts::where('id', $getSurveyAttemptInfo['id'])->update($data);
                            }
                            else
                            {    
                                //Getting surveyAttempt entry by $surveyAttemptID
                               SurveyAttempts::where('id', $getSurveyAttemptInfo['id'])->update($data);
                            } 
                            $totalRecords++;
                        }
                    }
                }
            }
        }
        echo "Total : ".$totalRecords." Records Updated......";
    }
    /**
     * [calculateLoi Calculate Loi for Survey Attempt and Recontact]
     * @author Gaurang Dangi
     * @Created Date   2019-07-03T17:37:31+0530
     * @param   [type] $attemptCreatedAt        [description]
     * @return  [type]                          [description]
     */
    private function calculateLoi($attemptCreatedAt)
    {
        return Carbon::now()->diffInMinutes($attemptCreatedAt);
    }
    /**
     * [changeVendorInvoiceStatus Cron to change vendor invoice status.]
     * @author Gaurang Dangi
     * @Created Date   2019-09-17T13:03:06+0530
     * @return  [type] [description]
     */
    public function changeVendorInvoiceStatus()
    {
    	$valuesForInvoice = '';
        $vendorInvoiceList  = new vendorInvoiceInfo;
        $vendorInvoiceList  = $vendorInvoiceList->select('id','status','due_date','invoice_amount')->whereIn('status',[1,3])->get();
        if ($vendorInvoiceList->count() > 0) 
        {
            foreach ($vendorInvoiceList as $invoices) 
            {
                if(Carbon::parse()->toDateString() > Carbon::parse($invoices->due_date)->toDateString())
                {

                    if($invoices->status == 1)
                    {
                        $valuesForInvoice = 
                        [
                            'status'        => 2,
                            'due_amount'     => $invoices->invoice_amount
                        ];    
                    }
                    else
                    {
                        $valuesForInvoice = 
                        [
                            'status'        => 2,
                        ];
                    }
                    $invoices->where('id',$invoices->id)->update($valuesForInvoice);
                    if($invoices->save())
                    {
                        $Success = 1;
                        $Message = "Record has been updated successfully.";
                    }
                    else
                    {
                        $Success = 1;
                        $Message = "Something went wrong!";
                    }
                    echo "<br>".$Message;
                }
            }
        } 
    }
    /* Start code for CronPjctVendorQuatasCompletes */
        /**
         * [CronPjctVendorQuatasCompletes Send Email to Project manager and Operation Team when Vendor Complete Quatas Completed]
         * @author Gaurang Dangi
         * @Created Date 2019-12-09T14:40:39+0530
         */
        public function CronPjctVendorQuatasCompletes()
        {           
            $toEmails    = array(); 
            $vendorArray = array();
            $vendorDetails = array();
            //Get project Details from database.
            $projects = Project::select('projects.project_id','projects.project_reference_name','projects.is_recontact','projects.project_manager_id','projects.invoice_id','users.email','users.name')->orderBy('project_id','asc')->join('users',"projects.project_manager_id","=","users.id")->where('projects.status',2)->get();
            
            foreach ($projects as $prjct) 
            {
                $prjctNames = $prjct->invoiceRecord()->select('project_name','epo_number')->get();
                $EPONumber = ($prjctNames && $prjctNames[0]->epo_number)?$prjctNames[0]->epo_number:'';
                //Get vendor details which status is active.
                $vendors =  $prjct->projectVendors()->select('id','vendor_id','required_completes','vendor_count_id')->where('active',1)->get();
                foreach ($vendors as $vendor) 
                {
                    //vendor required completes and completes attempts are matched that value store in array.
                    if($vendor->required_completes <= $vendor->completeAttemptsCount($prjct['is_recontact']))
                    {
                        $VendorName = ($vendor->company()['company_nickname'])?$vendor->company()['company_nickname']:$vendor->company()['company_name'];
                        $vendorArray['id']                    = $vendor->id;    
                        $vendorArray['vendor_id']             = $vendor->vendor_id;    
                        $vendorArray['vendor_name']           = $VendorName.'-'.$vendor->vendor_count_id;
                        $vendorArray['required_completes']    = $vendor->required_completes;    
                        $vendorArray['completeAttemptsCount'] = $vendor->completeAttemptsCount($prjct['is_recontact']);    
                        $vendorArray['manager_id']            = $prjct->email;    
                        $vendorArray['project_id']            = $prjct->project_id;    
                        $vendorArray['project_names']         = $prjctNames[0]->project_name.'_'.$prjct->project_reference_name;        
                        $vendorArray['EPO_Number']            = $EPONumber;        
                        $vendorArray['PM_Name']               = ucfirst($prjct->name);        
                        $vendorDetails[]                      = $vendorArray;
                    }                    
                }
            }
            foreach ($vendorDetails as $value) 
            {
                $emailSubject = '';
                $toEmails    = array(); 
                $mailText       = ''; 
                $mailText       =   "Hi Team,\r\n\n";
                $emailSubject   =   "Elite Opinio: Quota Met for '".$value['vendor_name']."' Project '".$value['project_names']." ".$value['EPO_Number']." ' '".$value['PM_Name']."'";
                $mailText      .=   "Required number of completes from vendor '".$value['vendor_name']."' on Project '".$value['project_names']." ".$value['EPO_Number']."' has been delivered. Please go to Elite Opino and consider changing the value of 'Required Completes' if you wish to continue delivering from this vendor.\r\n\n";
                $toEmails      =   trim($value['manager_id']);
                Helper::sendEmail($emailSubject,$mailText,"Admin",$toEmails);
            }
        }        
    /* End code for CronPjctVendorQuatasCompletes */
    /**
     * [updateProjectIndexerDetails Cron for update the client and vendor cost details.]
     * @author Gaurang Dangi
     * @Created Date   2020-05-18T18:28:50+0530
     * @Updated Date   2020-05-18T18:28:50+0530
     * @return  [type] [description]
     */
    public function updateProjectIndexerDetails()
    {
        $projects = Project::select('project_id','rate_usd','cpi_usd','is_recontact','cpi','currency','invoice_id','extra_ids_count')->orderby('project_id','asc')->get();
        if ($projects->count() > 0) 
        {
            foreach ($projects as $project) 
            {
                $valuesForProjectCost = 
                [
                    'project_id'            =>    $project->project_id,
                    'invoice_id'            =>    $project->invoice_id,
                    'client_cost'           =>    $project->totalRevenueNew1($project->is_recontact),
                    'vendor_cost'           =>    $project->totalVendorsCostInfo($project->is_recontact),
                    'vendor_manual_cost'    =>    $project->totalVendorsProjectCost($project->is_recontact),
                    'updated_date'          =>    Carbon::now()->toDateTimeString(),
                ];
                DB::table('tbl_project_cost_details')->where('project_id',$project->project_id)->update($valuesForProjectCost); 
            }
            return "ok";
        }
        return "not ok";
    }
   public function updateVendorStatus(){
        $this->getInfo        = Helper::getVendorDetails();

        $vendorDetails = VendorProject::where(['vendor_id'=> $this->getInfo['disqoId'],'active'=>1])->get()->toArray();

        foreach ($vendorDetails as $key => $value) {
    
            $count = SurveyAttempts::where(['vendor_project_id'=> $value['id'],'status'=>'5'])->count();
            
            if($value['required_completes']==$count){
                Helper::changeDisqoPrjctStatus(1,$this->getInfo['disqoClientId'],$value['survey_number']);
                $vendor_active=VendorProject::find($value['id']);
                $vendor_active->active=0;
                $vendor_active->save();
                $projects = Project::select('projects.invoice_id','users.email','users.name')->join('users',"projects.project_manager_id","=","users.id")->where('projects.project_id',$value['project_id'])->first();
               
                $EPONumber = InvoiceRecords::select('epo_number')->where('invoice_id',trim($projects->invoice_id))->first();
                
               
                $emailSubject = '';
                $toEmails    = array(); 
                $mailText       = ''; 
                $mailText       =   "Hi Team,\r\n\n";
                $emailSubject   =   "DISQO QUOTA REACHED";
              
                $mailText      .=   "Quota of Disqo for Project ID - ".$value['project_id']."(".$EPONumber->epo_number.") is reached. Kindly take the appropriate action. \r\n\nLink -http://staging.eliteopinio.com/projects/edit/".$value['project_id']."\r\n\nVendor project ID -".$value['id']."\r\n\n";
                $toEmails      =   trim($projects->email);
                $ccEmailIds    = array('ankit.k@elicitresearch.com','ankit@elicitresearch.com');
                $text          = 1;
                Helper::sendEmail($emailSubject,$mailText,"Admin",$toEmails,'','','',$text);
            }
        }
        
    }
}
