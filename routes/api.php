<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['api']], function () {
	//User Sign Up
	Route::post('/userSignUp', 'Api\UserApiController@userSignUp');
	//User Authenticate Code.
	Route::post('/userAuthenticateCode', 'Api\UserApiController@userAuthenticateCode');
	//User Profile Update Information Code.
	Route::post('/userUpdate', 'Api\UserApiController@userUpdate');
});
