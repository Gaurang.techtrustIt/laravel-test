<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $users = factory(\App\User::class, 10)->create();
        $companies = factory(\App\Companies::class, 20)->create();
        $companiesIds = $companies->pluck('company_id');
        $clientCompanies = $companies->whereIn('company_type', ['CLIENT', 'BOTH']);

        $clientCompanies->each(function ($company) {
            $i = 4;
            while ($i > 0) {
                $company->invoiceRecords()->save(factory(\App\InvoiceRecords::class)->make());
                $i--;
            }
        });

        $invoiceRecords = \App\InvoiceRecords::all();

        $invoiceRecords->each(function ($invoice) {
            $i = rand(1, 4);
            while ($i > 0) {
                $invoice->projects()->save(factory(\App\Project::class)->make([
                    'client_id' => $invoice['company_id'],
                    'multiple_survey_link' => array_random([true, false]),
                    'unique_link' => array_random([true, false])
                ]));

                $i--;

            }
        });

        $projects = \App\Project::all();

        $projects->each(function ($project) {
            $vendorsIds = \App\Companies::where('company_type', 'VENDOR')->pluck('company_id')->toArray();
            $i = rand(1, 4);
            while ($i > 0) {
                $project->projectVendors()->save(factory(\App\VendorProject::class)->make([
                    'vendor_id' => array_random($vendorsIds),
                ]));
                $i--;
            }

            if($project->unique_link == true)
            {
                $project->unique_link_code = "dfweffw";
                $project->save();
            }

            if($project->multiple_survey_link == true)
            {
                $i = 10;
                while($i > 0)
                {
                    $project->projectSurveyLinks()->save(factory(\App\ProjectSurveyLink::class)->make([
                        'multiple_use' => false
                    ]));
                    $i--;
                }
            }

            if($project->multiple_survey_link == false)
            {

                    $project->projectSurveyLinks()->save(factory(\App\ProjectSurveyLink::class)->make([
                        'multiple_use' => true
                    ]));

            }





        });

        $vendorProjects = \App\VendorProject::all();
        $vendorProjects->each(function ($vendorProject) {
            $i = rand(1, 10);
            while ($i > 0) {
                $vendorProject->surveyAttempts()->save(factory(\App\SurveyAttempts::class)->make([
                    'project_id' => $vendorProject['project_id']
                ]));
                $i--;
            }
        });
    }
}
