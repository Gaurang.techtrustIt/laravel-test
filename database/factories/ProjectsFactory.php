<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'project_reference_name' => 'kunal',
        'country' => $faker->countryCode,
        'required_completes' => $faker->numberBetween(20, 100),
        'po_number' => $faker->postcode,
        'cpi' => 2,
        'loi' => 20,
        'ir' => 2,
        'note' => 'RANDOM NOTE',
        'status' => $faker->numberBetween(1, 6),
//        'pre_screener' => array_random([true, false]),
        'unique_ip' => array_random([true, false]),
        'unique_pid' => array_random([true, false]),
        'country_restricted' => array_random([true, false]),
        'project_type' => array_random(['B2B', 'B2C']),
        'project_manager_id' => $faker->numberBetween(1, 10)
    ];
});
