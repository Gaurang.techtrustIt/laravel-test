<?php

use Faker\Generator as Faker;
use App\Companies;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\InvoiceRecords::class, function (Faker $faker) {
    return [

        'epo_number' => 'EPO_' . $faker->unixTime,
        'project_name' => $faker->name . '_',

    ];
});
