<?php

use Faker\Generator as Faker;

$factory->define(App\ProjectSurveyLink::class, function (Faker $faker) {
    return [
        'survey_link' => 'http://google.com',
        'link_used' => array_random([true, false])
    ];
});
