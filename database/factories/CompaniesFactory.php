<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Companies::class, function (Faker $faker) {
    return [
        'company_name' => $faker->name,
        'company_type' => array_random(['CLIENT', 'VENDOR', 'BOTH']),
        'company_address' => $faker->streetAddress,
        'company_city' => $faker->city,
        'company_state' => 'lol',
        'company_country' => $faker->country,
        'company_pincode' => $faker->postcode,
        'company_primary_email' => $faker->companyEmail,
        'company_secondary_email' => $faker->companyEmail,
        'company_primary_contact_number' => $faker->phoneNumber,
        'company_secondary_contact_number' => $faker->phoneNumber,
        'note' => $faker->realText(10),
        'payment_cycle' => $faker->numberBetween(30, 90),
        'completion_link' => $faker->url,
        'disqualify_link' => $faker->url,
        'quotafull_link' => $faker->url,
        'added_by_user' => 1
    ];
});
