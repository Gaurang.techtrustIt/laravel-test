<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\VendorProject::class, function (Faker $faker) {


    return [
        'cpc' => $faker->numberBetween(1, 10),
        'required_completes' => $faker->numberBetween(50, 200),
        'notes' => 'RANDOM NOTES',
        'active' => array_random([true, false]),
        'completion_link' => 'http://google.com',
        'disqualify_link' => 'http://google.com',
        'quotafull_link' => 'http://google.com',
    ];
});
