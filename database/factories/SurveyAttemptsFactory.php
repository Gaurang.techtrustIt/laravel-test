<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\SurveyAttempts::class, function (Faker $faker) {
    return [
        'respondent_id' => str_random(10),
        'status' => rand(1,5),
        'respondent_ip_address' => str_random(10),
        'loi' => 0,
        'approved' => false
    ];
});
