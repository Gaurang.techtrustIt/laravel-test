<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyProjectsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('country')->nullable()->change();
            $table->integer('required_completes')->nullable()->change();
            $table->string('po_number')->nullable()->change();
            $table->float('cpi', 8, 2)->nullable()->change();
            $table->float('loi', 8, 2)->nullable()->change();
            $table->float('ir', 8, 2)->nullable()->change();
            //options type: B2B, B2C
            $table->string('project_type')->nullable()->change();
            $table->integer('status')->unsigned()->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
