<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyAttemptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('vendor_project_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->string('respondent_id');
            $table->integer('status');
            $table->string('respondent_ip_address');
            $table->float('loi', 8, 2);
            $table->boolean('loi_exceeded')->default(false);
            $table->boolean('speeder')->default(false);
            $table->boolean('approved')->default(false);

            $table->foreign('project_id')->references('project_id')->on('projects');
            $table->foreign('vendor_project_id')->references('id')->on('vendor_projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_attempts');
    }
}
