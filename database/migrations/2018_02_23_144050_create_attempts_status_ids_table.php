<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttemptsStatusIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attempts_status_ids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status_name')->unique();
        });

        // Insert some stuff

        $allStatusName = [
            'Abandon',
            'Disqualified',
            'Quotafull',
            'Duplicate IP',
            'Completed',
            'Duplicate PID',
            'Different Country',
            'Failed at Pre-Screening',
            'LOI Exceeded',
            'Speeder',
            'Invalid unique link code'
        ];

        foreach ($allStatusName as $status) {
            DB::table('attempts_status_ids')->insert(
                array(
                    'status_name' => $status
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attempts_status_ids');
    }
}
