<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorInvoiceTble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_invoice_info', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('invoice_number')->unsigned();
            $table->integer('vendor_id')->unsigned()->references('company_id')->on('companies');
            $table->integer('invoice_amount')->unsigned();
            $table->integer('user_id')->unsigned()->references('company_id')->on('companies');
            $table->string('invoice_name',255);
            $table->timestamp('date');
            $table->timestamp('due_date');
            $table->timestamps();
            $table->foreign('vendor_id')->references('company_id')->on('companies');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_invoice_info');
    }
}
