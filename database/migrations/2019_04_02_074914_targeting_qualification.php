<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TargetingQualification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema of qualification_age_master table.
        Schema::create('qualification_age_master', function (Blueprint $table) {
            $table->increments('id');        
            $table->string('age_value','150');                 
            $table->timestamps();
        });

        //Schema of qualification_employment_master table.
         Schema::create('qualification_employment_master', function (Blueprint $table) {
            $table->increments('id');        
            $table->string('employment_value','150');                 
            $table->timestamps();
        });

        //Schema of qualification_hhi_master table.
        Schema::create('qualification_hhi_master', function (Blueprint $table) {
            $table->increments('id');        
            $table->string('hhi_value','150')->comment('HouseHold Income Value');                 
            $table->timestamps();
        });

        //Schema of qualification_education_master table.
        Schema::create('qualification_education_master', function (Blueprint $table) {
            $table->increments('id');        
            $table->string('education_value','150');                 
            $table->timestamps();
        });

        //Schema of project_qualification_age_trans table.
        Schema::create('project_qualification_age_trans', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('project_id')->unsigned();
            $table->integer('vendor_id')->unsigned()->nullable();   
            $table->integer('qual_age_id')->unsigned();
            $table->foreign('project_id')->references('project_id')->on('projects')->comment('project_id');
            $table->foreign('vendor_id')->references('company_id')->on('companies')->comment('vendor_id');
            $table->foreign('qual_age_id')->references('id')->on('qualification_age_master')->comment('id of qualification_age_master');
            $table->timestamps();
        });

        Schema::create('project_qualification_gender_trans', function (Blueprint $table) {

            $table->increments('id');   
            $table->integer('project_id')->unsigned();
            $table->integer('vendor_id')->unsigned()->nullable();              
            $table->foreign('project_id')->references('project_id')->on('projects')->comment('project_id');
            $table->foreign('vendor_id')->references('company_id')->on('companies')->comment('vendor_id');
            $table->tinyInteger('qualification_gender')->comment('0 : Both , 1: Male , 2 : Female');   
            $table->timestamps();
            
        });

         Schema::create('project_qualification_education_trans', function (Blueprint $table) {

            $table->increments('id');   
            $table->integer('project_id')->unsigned();
            $table->integer('vendor_id')->unsigned()->nullable(); 
            $table->integer('qual_edu_id')->unsigned();   
            $table->foreign('project_id')->references('project_id')->on('projects')->comment('project_id');
            $table->foreign('vendor_id')->references('company_id')->on('companies')->comment('vendor_id');
            $table->foreign('qual_edu_id')->references('id')->on('qualification_education_master')->comment('id of qualification_education_master');     
            $table->timestamps();
            
        });

         Schema::create('project_qualification_employment_trans', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('vendor_id')->unsigned()->nullable();    
            $table->integer('qual_emp_id')->unsigned();
            $table->foreign('project_id')->references('project_id')->on('projects')->comment('project_id');
            $table->foreign('vendor_id')->references('company_id')->on('companies')->comment('vendor_id');
            $table->foreign('qual_emp_id')->references('id')->on('qualification_employment_master')->comment('id of qualification Employment master');  
            $table->timestamps();
            
        });

          Schema::create('project_qualification_postcode_trans', function (Blueprint $table) {

            $table->increments('id');   
            $table->integer('project_id')->unsigned();
            $table->integer('vendor_id')->unsigned()->nullable();  
            $table->foreign('project_id')->references('project_id')->on('projects')->comment('project_id');
            $table->foreign('vendor_id')->references('company_id')->on('companies')->comment('vendor_id');
            $table->text('postcode');
            $table->boolean('postcode_check')->default(false);
            $table->timestamps();

        });

          Schema::create('project_qualification_hhi_trans', function (Blueprint $table) {

            $table->increments('id');   
            $table->integer('project_id')->unsigned();
            $table->integer('vendor_id')->unsigned()->nullable();  
            $table->integer('qual_hhi_id')->unsigned();
            $table->foreign('project_id')->references('project_id')->on('projects')->comment('project_id');
            $table->foreign('vendor_id')->references('company_id')->on('companies')->comment('vendor_id');
            $table->foreign('qual_hhi_id')->references('id')->on('qualification_hhi_master')->comment('id of qualification_hhi_master');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualification_age_master');
        Schema::dropIfExists('qualification_employment_master');
        Schema::dropIfExists('qualification_hhi_master');
        Schema::dropIfExists('qualification_education_master');
        Schema::dropIfExists('project_qualification_age_trans');
        Schema::dropIfExists('project_qualification_gender_trans');
        Schema::dropIfExists('project_qualification_education_trans');
        Schema::dropIfExists('project_qualification_employment_trans');
        Schema::dropIfExists('project_qualification_postcode_trans');
        Schema::dropIfExists('project_qualification_hhi_trans');

    }
}
