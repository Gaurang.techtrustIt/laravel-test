<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecontactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recontact_survey_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_attempt_id')->unsigned();
            $table->integer('elr_survey_id')->unsigned();
            $table->integer('respondent_id')->unsigned();
            $table->string('survey_link');
            $table->timestamps();
            $table->foreign('survey_attempt_id')->references('id')->on('survey_attempts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recontact_survey_links');
    }
}
