<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecontactSurveyAttempts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('recontact_survey_attmepts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_attempt_id')->unsigned();
            $table->integer('elr_attempt_id')->unsigned();
            $table->string('respondent_id');
            $table->integer('vendor_project_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('status');
            $table->string('respondent_ip_address');
            $table->float('loi', 8, 2);
            $table->boolean('loi_exceeded')->default(false);
            $table->boolean('speeder')->default(false);
            $table->boolean('approved')->default(false);
            $table->timestamps();
            $table->foreign('survey_attempt_id')->references('id')->on('survey_attempts');
            $table->foreign('project_id')->references('project_id')->on('projects');
            $table->foreign('vendor_project_id')->references('id')->on('vendor_projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recontact_survey_attmepts');
    }
}
