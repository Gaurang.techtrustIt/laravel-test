<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('company_id');

            $table->string('company_name');
            // options  in company type: CLIENT VENDOR BOTH
            $table->string('company_type');
            $table->string('company_address');
            $table->string('company_city');
            $table->string('company_state');
            $table->string('company_country');
            $table->string('company_pincode');
            $table->string('company_primary_email');
            $table->string('company_secondary_email')->nullable();
            $table->string('company_primary_contact_number');
            $table->string('company_secondary_contact_number')->nullable();
            $table->string('note');
            $table->integer('payment_cycle');
            $table->string('completion_link')->nullable();
            $table->string('quotafull_link')->nullable();
            $table->string('disqualify_link')->nullable();
            $table->integer('added_by_user');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
