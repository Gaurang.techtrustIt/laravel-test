<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsStatusIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_status_ids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status_name')->unique();
        });

        // Insert some stuff

        $allStatusName = [
            'Pending launch',
            'Live',
            'Pause',
            'Pending IDs',
            'IDs received',
            'Cancelled',
        ];

        foreach ($allStatusName as $status) {
            DB::table('projects_status_ids')->insert(
                array(
                    'status_name' => $status
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_status_ids');
    }
}
