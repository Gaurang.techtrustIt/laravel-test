<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompleteLinkUniqueCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_records', function($table) {
            $table->text('complete_unique_link_code')->after('unique_link_code')->nullable();
        });
        Schema::table('projects', function($table) {
            $table->text('complete_unique_link_code')->after('unique_link_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_records', function($table) {
            $table->dropColumn('complete_unique_link_code');
        });
        Schema::table('projects', function($table) {
            $table->dropColumn('complete_unique_link_code');
        });
    }
}
