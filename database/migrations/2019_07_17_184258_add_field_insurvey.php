<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInsurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_attempts',function(Blueprint $table){
            $table->string('clixsense_respondent_id',255)->default(0);
        });
        Schema::table('recontact_survey_links',function(Blueprint $table){
            $table->string('clixsense_respondent_id',255)->default(0);
        });
        Schema::table('recontact_survey_attmepts',function(Blueprint $table){
            $table->string('clixsense_respondent_id',255)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Scheme::table('survey_attempts',function(Blueprint $table)
        {
            $table->dropColumn('clixsense_respondent_id');
        });
        Schema::table('recontact_survey_links',function(Blueprint $table){
            $table->string('clixsense_respondent_id',255)->default(0);
        });
        Scheme::table('recontact_survey_attmepts',function(Blueprint $table)
        {
            $table->dropColumn('clixsense_respondent_id');
        });
    }
}
