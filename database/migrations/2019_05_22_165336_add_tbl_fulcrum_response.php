<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTblFulcrumResponse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fulcrum_vendor_security_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pid');
            $table->string('status');
            $table->text('api_request');
            $table->string('api_resposne');
            $table->text('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fulcrum_vendor_security_details');
    }
}
