<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveylink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('dummy_survey_link', function (Blueprint $table) {
            $table->increments('id');
            $table->text('completion_link');
            $table->text('disqualify_link');
            $table->text('quotafull_link');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dummy_survey_link');
    }
}
