<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrescreenerQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescreener_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prescreener_id')->unsigned();
            $table->string('question');
            $table->boolean('multi_answer')->default(false);
            $table->timestamps();

            $table->foreign('prescreener_id')->references('id')->on('prescreeners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescreener_questions');
    }
}
