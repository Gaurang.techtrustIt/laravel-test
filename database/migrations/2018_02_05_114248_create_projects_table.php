<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('project_id');
            $table->timestamps();
            $table->string('project_reference_name');
            $table->integer('client_id')->unsigned();
            $table->string('country');
            $table->integer('required_completes');
            $table->string('po_number');
            $table->float('cpi', 8, 2);
            $table->float('loi', 8, 2);
            $table->float('ir', 8, 2);
            $table->string('note')->nullable();
            $table->integer('status')->unsigned();
            $table->integer('project_manager_id')->unsigned();
            $table->integer('invoice_id')->unsigned();
            //options type: B2B, B2C
            $table->string('project_type');
            $table->boolean('multiple_survey_link')->default(false);
            $table->integer('pre_screener')->unsigned()->nullable();
            $table->boolean('unique_ip')->default(false);
            $table->boolean('unique_pid')->default(false);
            $table->boolean('country_restricted')->default(false);
            $table->boolean('unique_link')->default(false);
            $table->string('unique_link_code')->default('alphaSecure');
            $table->integer('loi_limit')->default(0);
            $table->integer('speeder_limit')->default(0);


            $table->foreign('client_id')->references('company_id')->on('companies');
            $table->foreign('project_manager_id')->references('id')->on('users');
            $table->foreign('invoice_id')->references('invoice_id')->on('invoice_records');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
