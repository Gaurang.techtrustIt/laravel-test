<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('project_id')->unsigned();
            $table->integer('vendor_id')->unsigned();
            $table->float('cpc', 8, 2);
            $table->integer('required_completes')->unsigned();
            $table->string('notes');
            $table->boolean('active')->default(true);
            $table->string('completion_link');
            $table->string('disqualify_link');
            $table->string('quotafull_link');

            $table->foreign('project_id')->references('project_id')->on('projects');
            $table->foreign('vendor_id')->references('company_id')->on('companies');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_projects');
    }
}
