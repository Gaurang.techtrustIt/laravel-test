<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_categories', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('description');
            $table->string('home_path');
            $table->timestamps();

        });


        $roleDescriptionHomePath = [
            'Project Manager'  => '/projects/list',
            'Accounts Manager' => 'accounts/invoice/list',
            'Admin' => '/projects/list'
        ];

        foreach ($roleDescriptionHomePath as $description => $homePath) {
            DB::table('user_categories')->insert(
                array(
                    'description' => $description,
                    'home_path' => $homePath
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_categories');
    }
}
