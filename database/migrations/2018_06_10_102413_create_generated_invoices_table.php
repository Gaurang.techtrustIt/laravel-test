<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneratedInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generated_invoices', function (Blueprint $table) {
            $table->increments('id');

            $table->string('invoice_number');
            $table->string('project_name');
            $table->integer('client_id');
            $table->text('invoice_ids');
            $table->text('project_info');
            $table->text('contact_info');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generated_invoices');
    }
}
