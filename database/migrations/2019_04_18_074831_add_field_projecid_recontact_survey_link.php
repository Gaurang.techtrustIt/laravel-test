<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldProjecidRecontactSurveyLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recontact_survey_links', function (Blueprint $table) {
            $table->integer('projectId')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recontact_survey_links', function (Blueprint $table) {
            $table->dropColumn('projectId');
        });
    }
}
