<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indexers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned()->unique();
            $table->string('project_full_name');
            $table->float('avg_loi')->default(0);
            $table->float('avg_disqualified_loi')->default(0);
            $table->float('ir')->default(0);
            $table->integer('completes_count')->default(0);
            $table->integer('hits_count')->default(0);
            $table->integer('quotafull_count')->default(0);;
            $table->integer('abandon_count')->default(0);;
            $table->integer('blocked_count')->default(0);
            $table->integer('disqualified_count')->default(0);
            $table->integer('approved_count')->default(0);
            $table->float('vendor_cost')->default(0);
            $table->float('revenue')->default(0);
            $table->float('profit')->default(0);
            $table->float('avg_cpc')->default(0);
            $table->integer('manager_id')->unsigned();
            $table->integer('invoice_id')->unsigned();
            $table->timestamp('last_complete')->nullable();
            $table->timestamp('last_redirect')->nullable();

            $table->timestamps();


            $table->foreign('project_id')->references('project_id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indexers');
    }
}
