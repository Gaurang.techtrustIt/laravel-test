<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueLinkCodeInInvoiceRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_records', function (Blueprint $table) {
            $table->string('unique_link_code')->default('alphaSecure');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_records', function (Blueprint $table) {
            $table->dropColumn('unique_link_code');
        });

    }
}
