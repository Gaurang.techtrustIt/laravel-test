<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_records', function (Blueprint $table) {
            $table->increments('invoice_id');
            $table->string('epo_number')->unique();
            $table->string('project_name');
            $table->integer('company_id')->unsigned();
            $table->integer('status')->default(1);
            $table->integer('manager')->default(0);
            $table->date('invoice_date')->nullable();
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_records');
    }
}
