<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceRecordStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_record_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status_name')->unique();
        });

        // Insert some stuff

        $allStatusName = [
            'Live',
            'ID received',
            'Invoice sent',
            'Invoice generated',
            'Payment received',
            'Payment pending',
            'Invoice canceled',
            'Payment overdue'
        ];

        foreach ($allStatusName as $status) {
            DB::table('invoice_record_statuses')->insert(
                array(
                    'status_name' => $status
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_record_statuses');
    }
}
