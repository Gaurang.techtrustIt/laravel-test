<div class="container">
    <div class="row mt-5 pt-4">
        <div class="form-header blue-grey-text rounded p-1">
            <h3 class="">User</h3>
        </div>
        <form method="POST" action="{{ url('user/save') }}" id="vendorLoginForm">
            {{ csrf_field() }}
            <input type="text" class="id" name="id" style="display: none;" value="{{$id}}">
            <!-- Default input email -->
            <label class="grey-text font-weight-light">User Name</label>
            <input type="text" id="username" name="username" class="form-control" value="{{ old('username') }}" required autofocus>
            <br>
            @if ($errors->has('username'))
            <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
            <br>
            <br>
            @endif

            <br>

            <!-- Default input password -->
            <label for="password" class="grey-text font-weight-light">Password</label>
            <input type="password" id="password" name="password" class="form-control" required>
            <br>
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            <br>
            <br>
            @endif
            <br>
            <div class="text-center mt-4">
                <button class="btn btn-primary login" type="submit" id="login" name="Register">Register
                </button>
            </div>
        </form>

    </div>
</div>
