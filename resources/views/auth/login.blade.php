@extends('layouts.main')

@section('content')
<div class="bg-white mt-5">
    <div class="d-flex justify-content-center">

        <div class=" col-6">

            <div class="card mx-xl-5">
                <div class="card-body">

                    <!--Header-->
                    <div class="form-header blue-grey-text rounded p-1">
                        <h3 class="p-1"><i class="fas fa-sign-in-alt"></i> Login:</h3>
                    </div>

                    <hr>

                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <!-- Default input email -->
                        <label for="email" class="grey-text font-weight-light">Your email</label>
                        <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}"
                            required autofocus>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                        <br>

                        <!-- Default input password -->
                        <label for="password" class="grey-text font-weight-light">Your password</label>
                        <input type="password" id="password" name="password" class="form-control" required>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif

                        <div class="text-center mt-4">
                            <button class="btn btn-primary" type="submit">Login
                            </button>
                        </div>
                    </form>

                </div>

                <!--Footer-->
                <div class="modal-footer">
                    <div class="options font-weight-light">
                        <a class="blue-grey-text" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>

            </div>
            <!-- Grid column -->

        </div>

    </div>

</div>
@endsection